#include "Letter.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

int Letter::getRed(int I, int J)
{
	if (I == 0 && J == 0)
		return _00.at(0);
	else if (I == 1 && J == 0)
		return _10.at(0);
	else if (I == 2 && J == 0)
		return _20.at(0);
	else if (I == 0 && J == 1)
		return _01.at(0);
	else if (I == 1 && J == 1)
		return _11.at(0);
	else if (I == 2 && J == 1)
		return _21.at(0);
	else if (I == 0 && J == 2)
		return _02.at(0);
	else if (I == 1 && J == 2)
		return _12.at(0);
	else if (I == 2 && J == 2)
		return _22.at(0);
	else
	{
		cout << "ERROR: wrong I and J values" << endl;
		return 50;
	}
}

int Letter::getGreen(int I, int J)
{
	if (I == 0 && J == 0)
		return _00.at(1);
	else if (I == 1 && J == 0)
		return _10.at(1);
	else if (I == 2 && J == 0)
		return _20.at(1);
	else if (I == 0 && J == 1)
		return _01.at(1);
	else if (I == 1 && J == 1)
		return _11.at(1);
	else if (I == 2 && J == 1)
		return _21.at(1);
	else if (I == 0 && J == 2)
		return _02.at(1);
	else if (I == 1 && J == 2)
		return _12.at(1);
	else if (I == 2 && J == 2)
		return _22.at(1);
	else
	{
		cout << "ERROR: wrong I and J values" << endl;
		return 50;
	}
}

int Letter::getBlue(int I, int J)
{
	if (I == 0 && J == 0)
		return _00.at(2);
	else if (I == 1 && J == 0)
		return _10.at(2);
	else if(I == 2 && J == 0)
		return _20.at(2);
	else if(I == 0 && J == 1)
		return _01.at(2);
	else if(I == 1 && J == 1)
		return _11.at(2);
	else if(I == 2 && J == 1)
		return _21.at(2);
	else if(I == 0 && J == 2)
		return _02.at(2);
	else if(I == 1 && J == 2)
		return _12.at(2);
	else if (I == 2 && J == 2)
		return _22.at(2);
	else
	{
		cout << "ERROR: wrong I and J values" << endl;
		return 50;
	}
}

void Letter::assign(char letter, bool colourise)
{
	if (colourise == true)
	{
		_rn1 = rand() % 201;
		_rn2 = rand() % 201;
		_rn3 = rand() % 201;
		_rn4 = rand() % 201;
		_rn5 = rand() % 201;
		_rn6 = rand() % 201;
		_rn7 = rand() % 201;
		_rn8 = rand() % 201;
		_rn9 = rand() % 201;
	}
	else
	{
		_rn1 = 0;
		_rn2 = 0;
		_rn3 = 0;
		_rn4 = 0;
		_rn5 = 0;
		_rn6 = 0;
		_rn7 = 0;
		_rn8 = 0;
		_rn9 = 0;
	}


	_00.clear();
	_10.clear();
	_20.clear();
	_01.clear();
	_11.clear();
	_21.clear();
	_02.clear();
	_12.clear();
	_22.clear();

	if (letter == 'a')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(255);
			_10.push_back(_rn2);
			_20.push_back(255);
			_01.push_back(_rn4);
			_11.push_back(255);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'b')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(255);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(255);
		}
	}

	else if (letter == 'c')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(255);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'd')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(255);
			_01.push_back(_rn4);
			_11.push_back(255);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(255);
		}
	}

	else if (letter == 'e')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'f')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(255);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(255);
		}
	}

	else if (letter == 'g')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(255);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'h')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn8);
		}
	}

	else if (letter == 'i')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'j')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(255);
		}
	}

	else if (letter == 'k')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'l')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(255);
			_01.push_back(_rn4);
			_11.push_back(255);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'm')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'n')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(255);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'o')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'p')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(255);
		}
	}

	else if (letter == 'q')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'r')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(255);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 's')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(255);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(255);
		}
	}

	else if (letter == 't')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(_rn3);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(255);
			_12.push_back(_rn8);
			_22.push_back(255);
		}
	}

	else if (letter == 'u')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(255);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'v')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(_rn3);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(255);
			_12.push_back(_rn8);
			_22.push_back(255);
		}
	}

	else if (letter == 'w')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(_rn3);
			_01.push_back(_rn4);
			_11.push_back(_rn5);
			_21.push_back(_rn6);
			_02.push_back(_rn7);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else if (letter == 'x')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(_rn3);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(_rn9);
		}
	}
	
	else if (letter == 'y')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(255);
			_20.push_back(_rn3);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(_rn7);
			_12.push_back(255);
			_22.push_back(255);
		}
	}

	else if (letter == 'z')
	{
		for (int i = 0; i < 3; i++)
		{
			_rn1 += _rn1 * i;
			_rn2 += _rn2 * i;
			_rn3 += _rn3 * i;
			_rn4 += _rn4 * i;
			_rn5 += _rn5 * i;
			_rn6 += _rn6 * i;
			_rn7 += _rn7 * i;
			_rn8 += _rn8 * i;
			_rn9 += _rn9 * i;

			_00.push_back(_rn1);
			_10.push_back(_rn2);
			_20.push_back(255);
			_01.push_back(255);
			_11.push_back(_rn5);
			_21.push_back(255);
			_02.push_back(255);
			_12.push_back(_rn8);
			_22.push_back(_rn9);
		}
	}

	else
		cout << "ERROR: no letter match" << endl;
}