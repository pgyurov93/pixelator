#ifndef ENCRYPT_H
#define ENCRYPT_H
#include <string>
#include <string>
#include <cctype>
#include <algorithm>

#include "Letter.h"
#include "Message.h"
#include "EasyBMP.h"

using namespace std;

class Encrypt
{
private:
	Message _message;
	const char* _directory;
	int _imagewidth, _imageheight;
	int _linenumber, _charnumber;
	int _i_transformed, _j_transformed;
	char _char;
	Letter _letter;
	BMP _image;

public:
	Encrypt();
	void save_in(const char*);
	void encrypt_message(Message, bool, bool);
};




#endif ENCRYPT_H
