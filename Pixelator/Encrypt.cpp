#include "Encrypt.h"
#include <time.h>

Encrypt::Encrypt()
{
	//standard dir to save if not specified... not ideal atm
	_directory = "C:/Users/Petar/Google Drive/My Projects/Encryption/Pixelator/Pixelator/EncryptedImage.bmp"; //try to auto get some dir
}

void Encrypt::save_in(const char* savedir)
{
	_directory = savedir;
}

void Encrypt::encrypt_message(Message incoming_message, bool colourisation, bool invert)
{	
	srand(time(NULL)); //intialising seed for random numbers here

	string message = incoming_message.getMessage();
	int line_length = incoming_message.getLine_length();
	int number_of_lines = incoming_message.getNumber_of_lines();

	_imagewidth = line_length * 3;
	_imageheight = number_of_lines * 3; 

	_image.SetBitDepth(32);
	_image.SetSize(_imagewidth, _imageheight);

	for (int i = 0; i < message.size(); i++)
	{
		_char = message.at(i);

		if (_char == '\n')
		{
			_linenumber++;
			_charnumber = 0;
			continue;
		}

		if (   _char != '0' || _char != '1' || _char != '2'
			|| _char != '3' || _char != '4' || _char != '5'
			|| _char != '6' || _char != '7' || _char != '8' || _char != '9')
		{

			for (int height = _linenumber * 3; height < (_linenumber * 3 + 3); height++)
			{
				for (int width = _charnumber* 3; width < (_charnumber * 3 + 3); width++)
				{
					_i_transformed = width - (_charnumber * 3);
					_j_transformed = height - (_linenumber * 3);

					_letter.assign(_char, colourisation);

					_image(width, height)->Red = _letter.getRed(_i_transformed, _j_transformed);
					_image(width, height)->Green = _letter.getGreen(_i_transformed, _j_transformed);
					_image(width, height)->Blue = _letter.getBlue(_i_transformed, _j_transformed);
					_image(width, height)->Alpha = 0;
				}
			}
		}

		else
			continue;

		_charnumber++;
	}
	_image.WriteToFile(_directory);

	if (invert == true)
	{
		BMP newimage;
		newimage.ReadFromFile(_directory);

		for (int y = 0; y < newimage.TellHeight(); y++)
		{
			for (int x = 0; x < newimage.TellWidth(); x++)
			{
				int RED = newimage(x, y)->Red;
				int GREEN = newimage(x, y)->Green;
				int BLUE = newimage(x, y)->Blue;

				if (RED == 255 && GREEN == 255 && BLUE == 255)
				{
					newimage(x, y)->Red = rand() % 201;
					newimage(x, y)->Green = rand() % 201;
					newimage(x, y)->Blue = rand() % 201;
				}
				else
				{
					newimage(x, y)->Red = 255;
					newimage(x, y)->Green = 255;
					newimage(x, y)->Blue = 255;
				}
			}
		}

		newimage.WriteToFile("C:/Users/Petar/Google Drive/My Projects/Encryption/Pixelator/Pixelator/TEST.bmp");
	}


}